

	<section class="blog_posts">
		<div class="wrapper">
			<div class="title animated wow fadeIn">
				<h2>Recent Photos</h2>
				<h3>The most recent photos from me</h3>
				<hr class="separator"/>
			</div>

			<ul class="clearfix">
				<li class="animated wow fadeInDown">
					<div class="media">
						<div class="date">
							<span class="day">25</span>
							<span class="month">Jun</span>
						</div>
						<a href="#">
							<img src="<?php echo base_url(); ?>/img/blog_post1.jpg" alt=""/>
						</a>
					</div>
					<a href="#">
						<h1>Danau Weekuri , Sumba Barat ,NTT.</h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<div class="date">
							<span class="day">11</span>
							<span class="month">May</span>
						</div>
						<a href="#">
							<img src="<?php echo base_url(); ?>/img/blog_post2.jpg" alt=""/>
						</a>
					</div>					
					<a href="#">
						<h1>Pantai Mandorak, Sumba Barat,NTT.</h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".4s">
					<div class="media">
						<div class="date">
							<span class="day">13</span>
							<span class="month">Feb</span>
						</div>
						<a href="#">
							<img src="<?php echo base_url(); ?>/img/blog_post3.jpg" alt=""/>
						</a>
					</div>
					<a href="#">
						<h1>Padang Rumput ,Sumba Barat, NTT.</h1>
					</a>
				</li>

				<li class="animated wow fadeInDown" data-wow-delay=".6s">
					<div class="media">
						<div class="date">
							<span class="day">10</span>
							<span class="month">Jan</span>
						</div>
						<a href="#">
							<img src="<?php echo base_url(); ?>/img/blog_post4.jpg" alt=""/>
						</a>
					</div>
					<a href="#"><h1>Padang Rumput ,Sumba Barat, NTT.</h1>
				</a>
				</li>
			</ul>
		</div>
	</section><!--  End blog_posts  -->