
	
	<section class="testimonials wrapper">
		<div class="title animated wow fadeIn">
			<h2>Contact </h2>
			<h3>Do not hesitate to keep in touch with me</h3>
			<hr class="separator"/>
		</div>

		<ul class="clearfix">
			<li class="animated wow fadeInDown">
				
				<div class="client">
					<a href="https://www.facebook.com/rika.pehmanik"><img src="<?php echo base_url(); ?>/img/fb.png" class="avatar"/></a>
					<div class="client_details">
						<a href="https://www.facebook.com/rika.pehmanik"><h4>Facebook</h4></a>
						<h5>Rika Manik</h5>
					</div>
				</div>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				
				<div class="client">
					<a href="https://www.instagram.com/rika_manik/"><img src="<?php echo base_url(); ?>/img/insta.jpg" class="avatar"/></a>
					<div class="client_details">
						<a href="https://www.instagram.com/rika_manik/"><h4>Instagram</h4></a>
						<h5>rika_manik</h5>
					</div>
				</div>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				
				<div class="client">
					<a href="http://webrikacode3-jply.rhcloud.com/"><img src="<?php echo base_url(); ?>/img/blog.jpg" class="avatar"/></a>
					<div class="client_details">
						<a href="http://webrikacode3-jply.rhcloud.com/"><h4>Blog</h4></a>
						<h5>Rika Manik</h5>
					</div>
				</div>
			</li>
		</ul>

		<ul class="clearfix">
			<li class="animated wow fadeInDown">
				
				<div class="client">
					<a href="mailto:rikapriya@gmail.com"><img src="<?php echo base_url(); ?>/img/email.jpg" class="avatar"/></a>
					<div class="client_details">
						<a href="mailto:rikapriya@gmail.com"><h4>Email</h4></a>
						<h5>Rika Manik</h5>
					</div>
				</div>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				
				<div class="client">
					<a href="http://twitter.com/rika_priyanti"><img src="<?php echo base_url(); ?>/img/twitt.png" class="avatar"/></a>
					<div class="client_details">
						<a href="http://twitter.com/rika_priyanti"><h4>Twitter</h4></a>
						<h5>rika_priyanti</h5>
					</div>
				</div>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				
				<div class="client">
					<img src="<?php echo base_url(); ?>/img/cp.jpg" class="avatar"/>
					<div class="client_details">
						<h4>Contact Person</h4>
						<h5>081262770068</h5>
					</div>
				</div>
			</li>
		</ul>
	</section><!--  End testimonials  -->





















