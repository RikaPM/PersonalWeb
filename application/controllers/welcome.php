<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{
		
		$this->load->view('home');
	}
	public function work()
	{
		$isi['content'] = 'work';
		$this->load->view('index',$isi);
	}
	public function photo()
	{
		
		$isi['content'] = 'photo';
		$this->load->view('index', $isi);
	}

	public function portofolio()
	{
	
		$isi['content'] = 'portofolio';
		$this->load->view('index', $isi);
	}

	public function contact()
	{
		
		$isi['content'] = 'contact';
		$this->load->view('index', $isi);
	}

	
}
