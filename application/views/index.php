<!DOCTYPE html>
<html lang="en">
<head>
	<title>Rika PM- My Personal Web</title>
	<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="description" content="Sublime Stunning free HTML5/CSS3 website template"/>



	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/fancybox-thumbs.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/fancybox-buttons.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/fancybox.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main.css">


    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox-buttons.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox-media.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/fancybox-thumbs.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/wow.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/main.js"></script>
</head>
<body>




	<section class="billboard light">
		<header class="wrapper light">
			<a href="#"><img class="logo" src="<?php echo base_url(); ?>/img/logo_light.png" alt=""/></a>
			<nav>
				<ul>
					<li><a href="<?php echo site_url('welcome');?>">Home</a></li>
					<li><a href="<?php echo site_url('welcome/work');?>">Work</a></li>
					
					<li><a href="<?php echo site_url('welcome/photo');?>">Photo</a></li>
					<li><a href="<?php echo site_url('welcome/portofolio');?>">Portofolio</a></li>
					<li><a href="<?php echo site_url('welcome/contact');?>">Contact Me</a></li>
				</ul>
			</nav>
		</header>

		<div class="caption light animated wow fadeInDown clearfix">
			<h1>Rika Priyanti Manik</h1>
			<p>Fused with Nature</p>
			
		</div>
		<div class="shadow"></div>
	</section><!--  End billboard  -->




<?php $this->load->view($content); ?>
 <?php $this->load->view('footer'); ?>
</body>
</html>