
	<section class="testimonials wrapper">
		<div class="title animated wow fadeIn">
			<h2>Work </h2>
			<h3>What i'm doing right now</h3>
			<hr class="separator"/>
		</div>

		<ul class="clearfix">
			<li class="animated wow fadeInDown">
				<p><img src="<?php echo base_url(); ?>/img/quotes.png" alt="" class="quotes"/>Display is a press agency in a Computer Science Department, Brawijaya University, East Java.I join as a member in a Human Resource Development Department .
				<span class="triangle"></span>
				</p>
				<div class="client">
					<a href="display.ub.ac.id"><img src="<?php echo base_url(); ?>/img/client1.jpg" class="avatar"/></a>
					<div class="client_details">
						<a href="http://display.ub.ac.id"><h4>Display</h4></a>
						<h5>PSDM</h5>
					</div>
				</div>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".2s">
				<p><a href="http://www.lpmi.org/"><img src="<?php echo base_url(); ?>/img/quotes.png" alt="" class="quotes"/></a>ISSA stands for Indonesian student service agency.ISSA is an Christian agency that moves in part of Great Commission of Lord Jesus Christ.I join as a volunteer in ISSA.
				<span class="triangle"></span>
				</p>
				<div class="client">
					<img src="<?php echo base_url(); ?>/img/client2.jpg" class="avatar"/>
					<div class="client_details">
						<a href="http://www.lpmi.org/"><h4>ISSA</h4></a>
						<h5>Volunteer</h5>
					</div>
				</div>
			</li>
			<li class="animated wow fadeInDown"  data-wow-delay=".4s">
				<p><img src="<?php echo base_url(); ?>/img/quotes.png" alt="" class="quotes"/>CV Profile Image is a company of software house.I join as a student internships in sixth semester in field of Business Process Analysis and Web Development.
				<span class="triangle"></span>
				</p>
				<div class="client">
					<img src="<?php echo base_url(); ?>/img/client3.jpg" class="avatar"/>
					<div class="client_details">
						<h4>CV Profile Image</h4>
						<h5>Student Internships</h5>
					</div>
				</div>
			</li>
		</ul>
	</section><!--  End testimonials  -->


